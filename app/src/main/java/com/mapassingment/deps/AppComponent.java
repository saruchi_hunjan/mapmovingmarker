package com.mapassingment.deps;

import com.mapassingment.component.home.HomeActivity;
import com.mapassingment.component.login.LoginActivity;
import com.mapassingment.networking.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by saruchiarora on 1/20/18.
 */
@Singleton
@Component(modules = {NetworkModule.class,MainModule.class})
public interface AppComponent {
    void inject(LoginActivity loginActivity);
    void inject(HomeActivity homeActivity);

}
