package com.mapassingment.deps;

import android.arch.persistence.room.Room;
import android.content.Context;


import com.mapassingment.database.local.GpsDAO;
import com.mapassingment.database.local.GpsDatabase;
import com.mapassingment.database.repository.LocalRepository;
import com.mapassingment.database.repository.LocalRepositoryImp;
import com.mapassingment.database.repository.RemoteRepository;
import com.mapassingment.database.repository.RemoteRepositoryImp;
import com.mapassingment.networking.NetworkService;

import java.util.concurrent.Executor;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import static com.mapassingment.util.AppConstants.DATABASE_NAME;

/**
 * Created by saruchiarora on 1/20/18.
 */
@Module
public class MainModule {
    private Context context;


    public MainModule(Context ctx){
        context = ctx;
    }
    @Provides
    public GpsDAO getGpsDAO(GpsDatabase database){
        return database.gpsDAO();
    }

    @Provides
    public GpsDatabase getDatabase(){
        return Room.databaseBuilder(context.getApplicationContext(),
                GpsDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }
    @Provides
    public LocalRepository getLocalRepo(GpsDAO gpsDAO, Executor exec){
        return new LocalRepositoryImp(gpsDAO, exec);
    }

    @Provides
    public CompositeDisposable getVMCompositeDisposable(){
        return new CompositeDisposable();
    }
    @Provides
    public RemoteRepository getRemoteRepo(NetworkService networkService){
        return new RemoteRepositoryImp(networkService);
    }

}
