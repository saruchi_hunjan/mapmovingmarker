package com.mapassingment.database.repository;

import com.mapassingment.model.GpsResponse;
import com.mapassingment.model.LoginResponse;
import com.mapassingment.networking.NetworkService;

import io.reactivex.Observable;
/**
 * Created by saruchiarora on 1/20/18.
 */
public class RemoteRepositoryImp implements RemoteRepository {
    private NetworkService networkService;

    public RemoteRepositoryImp(NetworkService networkService) {
        this.networkService = networkService;
    }

    @Override
    public Observable<GpsResponse> getGPSData() {
        return networkService.getGpsData();
    }

    @Override
    public Observable<LoginResponse> login(String username, String password) {
        return networkService.getLoginResponse(username,password);
    }


}
