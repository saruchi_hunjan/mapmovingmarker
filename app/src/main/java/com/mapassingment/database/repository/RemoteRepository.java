package com.mapassingment.database.repository;

import com.mapassingment.model.GpsResponse;
import com.mapassingment.model.LoginResponse;
import com.mapassingment.networking.Response;

import org.json.JSONObject;

import io.reactivex.Observable;
/**
 * Created by saruchiarora on 1/20/18.
 */
public interface RemoteRepository {

     Observable<GpsResponse> getGPSData();
    Observable<LoginResponse> login(String username, String password);
}
