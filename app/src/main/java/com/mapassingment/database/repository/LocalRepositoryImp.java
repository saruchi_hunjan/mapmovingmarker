package com.mapassingment.database.repository;

import com.mapassingment.database.local.GpsDAO;
import com.mapassingment.database.local.GpsEntity;

import java.util.List;
import java.util.concurrent.Executor;

import io.reactivex.Flowable;
/**
 * Created by saruchiarora on 1/20/18.
 */
public class LocalRepositoryImp implements LocalRepository {
    private GpsDAO gpsDAO;
    private Executor executor;

    public LocalRepositoryImp(GpsDAO cDAO, Executor exec) {
        gpsDAO = cDAO;
        executor = exec;
    }
    @Override
    public Flowable<List<GpsEntity>> getGpsData() {
        return gpsDAO.getGpsData();
    }

    @Override
    public void insertData(final GpsEntity gpsEntity) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    gpsDAO.insertRecord(gpsEntity);
                }
            });
    }



    @Override
    public void deleteAll() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                gpsDAO.deleteEntity();

            }
        });
    }


}
