package com.mapassingment.database.repository;

import com.mapassingment.database.local.GpsEntity;

import java.util.List;

import io.reactivex.Flowable;
/**
 * Created by saruchiarora on 1/20/18.
 */
public interface LocalRepository {
    Flowable<List<GpsEntity>> getGpsData();
    void insertData(GpsEntity entity);
    void deleteAll();

}