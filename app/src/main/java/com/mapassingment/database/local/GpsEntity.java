package com.mapassingment.database.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saruchiarora on 1/20/18.
 */
@Entity
public class GpsEntity {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("DurationCommAlert")
    @Expose
    private float durationCommAlert;
    @SerializedName("DigitalIOStatus")
    @Expose
    private String digitalIOStatus;
    @SerializedName("HasAlert")
    @Expose
    private Boolean hasAlert;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public float getDurationCommAlert() {
        return durationCommAlert;
    }

    public void setDurationCommAlert(float durationCommAlert) {
        this.durationCommAlert = durationCommAlert;
    }

    public String getDigitalIOStatus() {
        return digitalIOStatus;
    }

    public void setDigitalIOStatus(String digitalIOStatus) {
        this.digitalIOStatus = digitalIOStatus;
    }

    public Boolean getHasAlert() {
        return hasAlert;
    }

    public void setHasAlert(Boolean hasAlert) {
        this.hasAlert = hasAlert;
    }

}
