package com.mapassingment.database.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by saruchiarora on 1/20/18.
 */
@Dao
public interface GpsDAO {
    // there is any change in the flowable will automatically Update The UI
    @Query("SELECT * FROM GpsEntity ")
    Flowable<List<GpsEntity>> getGpsData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRecord(GpsEntity coupon);

    @Query("DELETE FROM GpsEntity")
    void deleteEntity();
}
