package com.mapassingment.database.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by saruchiarora on 1/20/18.
 */

@Database(entities = {GpsEntity.class},version = 1)
public abstract class GpsDatabase  extends RoomDatabase {
    public abstract GpsDAO gpsDAO();
}
