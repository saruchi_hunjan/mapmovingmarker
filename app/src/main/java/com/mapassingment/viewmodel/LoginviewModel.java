package com.mapassingment.viewmodel;

import android.util.Log;

import com.mapassingment.base.listner.BaseCallback;
import com.mapassingment.database.repository.LocalRepository;
import com.mapassingment.database.repository.RemoteRepository;
import com.mapassingment.model.LoginResponse;
import com.mapassingment.networking.NetworkError;
import com.mapassingment.networking.Response;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by saruchiarora on 1/20/18.
 */

public class LoginviewModel {
    private RemoteRepository remoteRepository;
    private CompositeDisposable compositeDisposable;




    @Inject
    public LoginviewModel( RemoteRepository remoteRepo, CompositeDisposable disposable){
        remoteRepository = remoteRepo;
        compositeDisposable = disposable;
    }



    public void getLoginResponseFromService(final String username , final String password , final BaseCallback baseCallback){
        //add observable to CompositeDisposable so that it can be dispose when ViewModel is ready to be destroyed
        compositeDisposable.add(io.reactivex.Observable.just(1)
                .flatMap(new Function<Integer, Observable<LoginResponse>>() {
                    @Override
                    public Observable<LoginResponse> apply(Integer i) throws Exception {
                        return remoteRepository.login(username,password);
                    }
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<LoginResponse>() {
                    @Override
                    public void accept(LoginResponse response) throws Exception {
                        baseCallback.onSuccess(response);
                    }
    }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        baseCallback.onFail(new NetworkError(throwable));
                        Log.e("HomeActivity", "exception getting list", throwable);
                    }
                }));



    }

    public void unSubscribe() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.clear();
        }
    }
}
