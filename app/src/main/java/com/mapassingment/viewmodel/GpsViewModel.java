package com.mapassingment.viewmodel;

import android.util.Log;

import com.mapassingment.base.listner.BaseCallback;
import com.mapassingment.database.local.GpsEntity;
import com.mapassingment.database.repository.LocalRepository;
import com.mapassingment.database.repository.RemoteRepository;
import com.mapassingment.model.GpsResponse;
import com.mapassingment.model.TripDatum;
import com.mapassingment.networking.NetworkError;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by saruchiarora on 1/20/18.
 */

public class GpsViewModel {
        private LocalRepository localRepository;

        private RemoteRepository remoteRepository;

        private CompositeDisposable compositeDisposable;


        @Inject
        public GpsViewModel(LocalRepository localRepo, RemoteRepository remoteRepo, CompositeDisposable disposable){
            localRepository = localRepo;
            remoteRepository = remoteRepo;
            compositeDisposable = disposable;
        }
        public Flowable<List<GpsEntity>> getGpsData(){
            return localRepository.getGpsData();
        }


        public void deleteAlldata(){
            localRepository.deleteAll();
        }

        public void getGpsDataFromService(final BaseCallback baseCallback){
            //add observable to CompositeDisposable so that it can be dispose when ViewModel is ready to be destroyed
            //Call retrofit client on background thread and update database with response from service using Room
            deleteAlldata(); // deleting all records before inserting fresh data.

            compositeDisposable.add(io.reactivex.Observable.just(1)
                    .subscribeOn(Schedulers.computation())
                    .flatMap(new Function<Integer, Observable<GpsResponse>>() {
                        @Override
                        public Observable<GpsResponse> apply(Integer i) throws Exception {
                            return remoteRepository.getGPSData();
                        }
                    }).subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<GpsResponse>() {
                        @Override
                        public void accept(GpsResponse gpsResponse) throws Exception {
                            for(TripDatum tripDatum : gpsResponse.getTripData()){
                                for(GpsEntity entity : tripDatum.getTripList()){
                                    //database update
                                    localRepository.insertData(entity);
                                }


                            }

                            baseCallback.onSuccess(gpsResponse);
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            baseCallback.onFail(new NetworkError(throwable));
                            Log.e("HomeActivity", "exception getting list", throwable);
                        }
                    }));

        }



        public void unSubscribe() {
            if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
                compositeDisposable.clear();
            }
        }

}
