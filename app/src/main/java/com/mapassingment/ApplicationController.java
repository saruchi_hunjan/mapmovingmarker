package com.mapassingment;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.mapassingment.deps.AppComponent;
import com.mapassingment.deps.DaggerAppComponent;
import com.mapassingment.deps.MainModule;
import com.mapassingment.networking.NetworkModule;

import java.io.File;

/**
 * Created by saruchiarora on 1/20/18.
 */

public class ApplicationController extends Application {

    private static AppComponent component;
    private static Context context;


    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        context = getApplicationContext();
        File cacheFile = new File(getCacheDir(), "responses");

        component = DaggerAppComponent.builder()
                .networkModule(new NetworkModule(cacheFile))
                .mainModule(new MainModule(getApplicationContext()))
                .build();
        ;

    }


    public static Context getContext() {
        return context;
    }

    public static AppComponent getAppComponent() {
        return component;
    }

}

