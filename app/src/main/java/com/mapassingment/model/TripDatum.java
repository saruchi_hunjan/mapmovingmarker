
package com.mapassingment.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mapassingment.database.local.GpsEntity;
/**
 * Created by saruchiarora on 1/20/18.
 */
public class TripDatum {

@SerializedName("TripNumber")
@Expose
private Integer tripNumber;
@SerializedName("TripName")
@Expose
private String tripName;
@SerializedName("ColorCode")
@Expose
private String colorCode;
@SerializedName("TripList")
@Expose
private List<GpsEntity> tripList = null;

public Integer getTripNumber() {
return tripNumber;
}

public void setTripNumber(Integer tripNumber) {
this.tripNumber = tripNumber;
}

public String getTripName() {
return tripName;
}

public void setTripName(String tripName) {
this.tripName = tripName;
}

public String getColorCode() {
return colorCode;
}

public void setColorCode(String colorCode) {
this.colorCode = colorCode;
}

public List<GpsEntity> getTripList() {
return tripList;
}

public void setTripList(List<GpsEntity> tripList) {
this.tripList = tripList;
}

}