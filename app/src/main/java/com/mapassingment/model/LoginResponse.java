package com.mapassingment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by saruchiarora on 1/20/18.
 */
public class LoginResponse {

@SerializedName("UserID")
@Expose
private String userID;
@SerializedName("PersonID")
@Expose
private Object personID;
@SerializedName("AssetOwnerID")
@Expose
private String assetOwnerID;
@SerializedName("CustomerID")
@Expose
private Integer customerID;
@SerializedName("UserName")
@Expose
private String userName;
@SerializedName("EmailId")
@Expose
private String emailId;
@SerializedName("Photo")
@Expose
private Object photo;
@SerializedName("Success")
@Expose
private Boolean success;
@SerializedName("VehiclesCount")
@Expose
private Integer vehiclesCount;
@SerializedName("PersonsCount")
@Expose
private Integer personsCount;
@SerializedName("DGSetsCount")
@Expose
private Integer dGSetsCount;
@SerializedName("TrackVehicles")
@Expose
private Boolean trackVehicles;
@SerializedName("TrackPeople")
@Expose
private Boolean trackPeople;
@SerializedName("TrackDGSets")
@Expose
private Boolean trackDGSets;
@SerializedName("TrackGenericAssets")
@Expose
private Boolean trackGenericAssets;
@SerializedName("Token")
@Expose
private Object token;

public String getUserID() {
return userID;
}

public void setUserID(String userID) {
this.userID = userID;
}

public Object getPersonID() {
return personID;
}

public void setPersonID(Object personID) {
this.personID = personID;
}

public String getAssetOwnerID() {
return assetOwnerID;
}

public void setAssetOwnerID(String assetOwnerID) {
this.assetOwnerID = assetOwnerID;
}

public Integer getCustomerID() {
return customerID;
}

public void setCustomerID(Integer customerID) {
this.customerID = customerID;
}

public String getUserName() {
return userName;
}

public void setUserName(String userName) {
this.userName = userName;
}

public String getEmailId() {
return emailId;
}

public void setEmailId(String emailId) {
this.emailId = emailId;
}

public Object getPhoto() {
return photo;
}

public void setPhoto(Object photo) {
this.photo = photo;
}

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public Integer getVehiclesCount() {
return vehiclesCount;
}

public void setVehiclesCount(Integer vehiclesCount) {
this.vehiclesCount = vehiclesCount;
}

public Integer getPersonsCount() {
return personsCount;
}

public void setPersonsCount(Integer personsCount) {
this.personsCount = personsCount;
}

public Integer getDGSetsCount() {
return dGSetsCount;
}

public void setDGSetsCount(Integer dGSetsCount) {
this.dGSetsCount = dGSetsCount;
}

public Boolean getTrackVehicles() {
return trackVehicles;
}

public void setTrackVehicles(Boolean trackVehicles) {
this.trackVehicles = trackVehicles;
}

public Boolean getTrackPeople() {
return trackPeople;
}

public void setTrackPeople(Boolean trackPeople) {
this.trackPeople = trackPeople;
}

public Boolean getTrackDGSets() {
return trackDGSets;
}

public void setTrackDGSets(Boolean trackDGSets) {
this.trackDGSets = trackDGSets;
}

public Boolean getTrackGenericAssets() {
return trackGenericAssets;
}

public void setTrackGenericAssets(Boolean trackGenericAssets) {
this.trackGenericAssets = trackGenericAssets;
}

public Object getToken() {
return token;
}

public void setToken(Object token) {
this.token = token;
}

}
