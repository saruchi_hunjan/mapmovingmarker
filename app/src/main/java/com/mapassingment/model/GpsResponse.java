package com.mapassingment.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by saruchiarora on 1/20/18.
 */
public class GpsResponse {

@SerializedName("Success")
@Expose
private Boolean success;
@SerializedName("TripData")
@Expose
private List<TripDatum> tripData = null;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public List<TripDatum> getTripData() {
return tripData;
}

public void setTripData(List<TripDatum> tripData) {
this.tripData = tripData;
}

}