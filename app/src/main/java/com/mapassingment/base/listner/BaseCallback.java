package com.mapassingment.base.listner;

import com.mapassingment.networking.NetworkError;
/**
 * Created by saruchiarora on 1/20/18.
 */
public interface BaseCallback {
    void onSuccess(Object  response);

    void onFail(NetworkError networkError);
}