package com.mapassingment.base.listner;
/**
 * Created by saruchiarora on 1/20/18.
 */
public interface ActionBarView {

    void setUpIconVisibility(boolean visible);

    void setToolbarTitle(String titleKey);

}
