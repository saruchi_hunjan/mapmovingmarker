package com.mapassingment.base;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.mapassingment.base.listner.ActionBarView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import static com.mapassingment.util.ObjectUtil.isEmpty;
import static com.mapassingment.util.ObjectUtil.isNull;
/**
 * Created by saruchiarora on 1/20/18.
 */
public abstract class BaseActivity extends AppCompatActivity implements ActionBarView {
    public BasePresenter presenter;
    private Unbinder unbinder;
    protected Resources mResources;
    protected abstract void initializeDagger();
    protected abstract void initializePresenter();
    public abstract int getLayoutId();
    protected abstract void setUpUI();
    ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
        mResources= getResources();
        initializeDagger();
        initializePresenter();
        setUpUI();
        if (presenter != null) {
            presenter.initialize(getIntent().getExtras());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (presenter != null) {
            presenter.start();
        }
    }

    @Override
    public void setUpIconVisibility(boolean visible) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(visible);
        }
    }

    @Override
    public void setToolbarTitle(String titleKey) {
        final ActionBar actionBar = getSupportActionBar();
        if (!isNull(actionBar)) {
            if (!isEmpty(titleKey)) {
                actionBar.setTitle(titleKey);
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.finalizeView();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
