package com.mapassingment.base;

import android.os.Bundle;


import com.mapassingment.base.listner.BaseView;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;
/**
 * Created by saruchiarora on 1/20/18.
 */
public abstract class BasePresenter<T extends BaseView> {

    private WeakReference<T> view;

    protected AtomicBoolean isViewAlive = new AtomicBoolean();

    public T getView() {
        return view.get();
    }

    public void setView(T view) {
        this.view = new WeakReference<>(view);
    }

    public void initialize(Bundle extras) {
    }

    public void start() {
        isViewAlive.set(true);
    }

    public void finalizeView() {
        isViewAlive.set(false);
    }
}