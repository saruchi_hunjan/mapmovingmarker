package com.mapassingment.networking;


import com.mapassingment.model.GpsResponse;
import com.mapassingment.model.LoginResponse;

import org.json.JSONObject;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by saruchiarora on 1/20/18.
 */
public interface NetworkService {


    @GET("http://gofenzbeta.azurewebsites.net/api/Mobile/GetTrackingData")
    Observable<GpsResponse> getGpsData();

    @POST("Login")
    @FormUrlEncoded
    Observable<LoginResponse> getLoginResponse(@Field("UserName") String username,
                                               @Field("Password") String password);



}
