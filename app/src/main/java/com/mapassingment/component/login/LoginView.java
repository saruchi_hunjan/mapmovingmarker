package com.mapassingment.component.login;

import com.mapassingment.base.listner.BaseView;

/**
 * Created by saruchiarora on 1/20/18.
 */

public class LoginView {
    interface View extends BaseView {
        void NavigateToMainScreen(boolean isSucess);
        void onFailure(String appErrorMessage);
        void setLoaderVisibility(boolean isVisible);

    }

    interface Presenter {
        void getLoginResponse(String username , String password);
        void unSubscribe();
    }
}
