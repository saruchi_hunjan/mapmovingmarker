package com.mapassingment.component.login;

import android.os.Bundle;

import com.mapassingment.base.BasePresenter;
import com.mapassingment.base.listner.BaseCallback;
import com.mapassingment.model.LoginResponse;
import com.mapassingment.networking.NetworkError;
import com.mapassingment.viewmodel.LoginviewModel;

import javax.inject.Inject;

/**
 * Created by saruchiarora on 1/20/18.
 */

public class LoginPresenter extends BasePresenter<LoginView.View> implements LoginView.Presenter {

    private LoginviewModel loginviewModel;

    @Inject
    public LoginPresenter(LoginviewModel loginviewModel) {
        this.loginviewModel = loginviewModel;
    }



    @Override
    public void getLoginResponse( String username , String password) {
        getView().setLoaderVisibility(true);
        loginviewModel.getLoginResponseFromService(username,password,baseCallback);
    }

    @Override
    public void unSubscribe() {
        loginviewModel.unSubscribe();

    }

    private BaseCallback baseCallback = new BaseCallback() {
        @Override
        public void onSuccess(Object response) {
            if(response instanceof LoginResponse){

                    getView().setLoaderVisibility(false);
                    getView().NavigateToMainScreen( ((LoginResponse) response).getSuccess());


            }


        }

        @Override
        public void onFail(NetworkError networkError) {
            getView().setLoaderVisibility(false);
            getView().onFailure(networkError.getAppErrorMessage());
        }
    };

}
