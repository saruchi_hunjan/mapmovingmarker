package com.mapassingment.component.login;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mapassingment.ApplicationController;
import com.mapassingment.R;
import com.mapassingment.base.BaseActivity;
import com.mapassingment.component.home.HomeActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static android.support.design.widget.Snackbar.LENGTH_SHORT;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.mapassingment.util.ObjectUtil.isEmpty;
import static com.mapassingment.util.ObjectUtil.isNull;

/**
 * Created by saruchiarora on 1/20/18.
 */

public class LoginActivity extends BaseActivity implements LoginView.View {
    @Inject
    LoginPresenter loginPresenter;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView (R.id.edittext_username)
    EditText etUsername;
    @BindView (R.id.edittext_password)
    EditText etpassword;

    @Override
    protected void initializeDagger() {
        ApplicationController app = (ApplicationController) getApplicationContext();
        app.getAppComponent().inject(LoginActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter = loginPresenter;
        presenter.setView(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void setUpUI() {

    }

    @Override
    public void NavigateToMainScreen(boolean isSucess) {
        if(isSucess){
            Toast.makeText(this,getString(R.string.message_login_scuessfull),Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, HomeActivity.class);
            startActivity(i);
            finish();


        }
        else{
            Toast.makeText(this,getString(R.string.error_valid_details),Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onFailure(String appErrorMessage) {
        Snackbar.make(etpassword, appErrorMessage, LENGTH_SHORT).show();

    }

    @Override
    public void setLoaderVisibility(boolean isVisible) {
        pbLoading.setVisibility(isVisible ? VISIBLE : GONE);
    }

    @OnClick(R.id.btn_login)
    void onLoginClick(){
        hideKeyboard();
        String username = etUsername.getText().toString().trim();
        String password = etpassword.getText().toString().trim();
        if (isNull(username) && isEmpty(username)) {
            Snackbar.make(etUsername, getString(R.string.error_field_required), LENGTH_SHORT).show();
        }
        else if (isNull(password) && isEmpty(password)){
            Snackbar.make(etpassword, getString(R.string.error_field_required), LENGTH_SHORT).show();
        }
        else{
            loginPresenter.getLoginResponse(username,password);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.unSubscribe();


    }

    private void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
