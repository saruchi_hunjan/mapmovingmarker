package com.mapassingment.component.home;

import com.mapassingment.base.listner.BaseView;
import com.mapassingment.database.local.GpsEntity;
import java.util.List;

/**
 * Created by saruchiarora on 1/20/18.
 */

public class HomeView {
    interface View extends BaseView {
        void UpdateUi(List<GpsEntity> list);
        void onFailure(String appErrorMessage);
        void setLoaderVisibility(boolean isVisible);

    }

    interface Presenter {
        void getGpsData();
        void unSubscribe();
    }
}
