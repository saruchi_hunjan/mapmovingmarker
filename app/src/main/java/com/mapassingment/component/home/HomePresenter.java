package com.mapassingment.component.home;

import android.os.Bundle;

import com.mapassingment.base.BasePresenter;
import com.mapassingment.base.listner.BaseCallback;
import com.mapassingment.database.local.GpsEntity;
import com.mapassingment.networking.NetworkError;
import com.mapassingment.viewmodel.GpsViewModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.mapassingment.util.ObjectUtil.isNull;

/**
 * Created by saruchiarora on 1/20/18.
 */

public class HomePresenter extends BasePresenter<HomeView.View> implements HomeView.Presenter {
    private GpsViewModel gpsViewModel;
    private CompositeDisposable disposeBag;
    private List<GpsEntity> dataList;

    @Inject
    public HomePresenter(GpsViewModel gpsViewModel) {
        this.gpsViewModel = gpsViewModel;
        disposeBag = new CompositeDisposable();


    }
    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
        getView().setLoaderVisibility(true);
        gpsViewModel.getGpsDataFromService(baseCallback);

    }

    @Override
    public void getGpsData() {
//call retrofit service to get latest data and update database
        //runs in the background thread
        getView().setLoaderVisibility(true);
        Disposable disposable = gpsViewModel.getGpsData() // fetching Data from Database
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<GpsEntity>>() {
                               @Override
                               public void accept(List<GpsEntity> gpsEntityList) throws Exception {
                                   dataList = gpsEntityList;

                                   if (!isNull(dataList) && !dataList.isEmpty()) {
                                       getView().UpdateUi(dataList);

                                   } else {

                                   }
                                   getView().setLoaderVisibility(false);

                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                getView().setLoaderVisibility(false);
                                getView().onFailure(throwable.getMessage().toString());
                            }
                        }
                        , new Action() {
                            @Override
                            public void run() throws Exception {
                                getView().setLoaderVisibility(false);
                            }

                        });

        disposeBag.add(disposable);
    }


    @Override
    public void unSubscribe() {
        gpsViewModel.unSubscribe();

    }

    private BaseCallback baseCallback = new BaseCallback() {
        @Override
        public void onSuccess(Object reponse) {
            getView().setLoaderVisibility(false);
            getGpsData();

        }

        @Override
        public void onFail(NetworkError networkError) {
            getView().setLoaderVisibility(false);
            getView().onFailure(networkError.getAppErrorMessage());
        }
    };


}
