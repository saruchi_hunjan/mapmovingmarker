package com.mapassingment.component.home;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.mapassingment.ApplicationController;
import com.mapassingment.R;
import com.mapassingment.base.BaseActivity;
import com.mapassingment.component.login.LoginActivity;
import com.mapassingment.database.local.GpsEntity;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.google.android.gms.maps.model.JointType.ROUND;

/**
 * Created by saruchiarora on 1/20/18.
 */
//
public class HomeActivity extends BaseActivity implements HomeView.View , OnMapReadyCallback
         {
             private static final String TAG = HomeActivity.class.getSimpleName();

             private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    @Inject HomePresenter homePresenter;
    SupportMapFragment mapFragment;
             private PolylineOptions polylineOptions, blackPolylineOptions;
             private Polyline blackPolyline, greyPolyLine;
    private GoogleMap mMap;
             private List<GpsEntity> datalist;
             private List<LatLng> polyLineList;
             private Handler handler;
             private int index, next;
             private double lat, lng;

             private Marker marker;
             private LatLng startPosition,endPosition;
             private float v;
             private LatLng intialMarker;
             private ArrayList<Marker> markers = new ArrayList<>();
             private static double changePositionBy = 0.00005;

             private static float angle;
             private boolean isMarkerRotating = false;
             private LatLng lastLocation;
             private AnimatorMarker animatorMarker = null;

             @Override
    protected void initializeDagger() {
        ApplicationController app = (ApplicationController) getApplicationContext();
        app.getAppComponent().inject(HomeActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter = homePresenter;
        presenter.setView(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void setUpUI() {
        datalist = new ArrayList<>();
        polyLineList = new ArrayList<>();
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
//        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            checkLocationPermission();
//        }
    }

             @Override
             public boolean onCreateOptionsMenu(Menu menu) {
                 getMenuInflater().inflate(R.menu.menu_home, menu);
                 return super.onCreateOptionsMenu(menu);
             }
             @Override
             public boolean onOptionsItemSelected(MenuItem item) {
                 int id = item.getItemId();
                 switch (id){
                     case R.id.action_stop:
                         animatorMarker.stopAnimation();
                         break;

                 }

                 return super.onOptionsItemSelected(item);
             }

             @Override
    public void UpdateUi(List<GpsEntity> list) {
        datalist =list;
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void setLoaderVisibility(boolean isVisible) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(false);
        mMap.setIndoorEnabled(false);
        mMap.setBuildingsEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.RED);
        polylineOptions.width(5);
        for(GpsEntity entity : datalist){
            double lat = Double.parseDouble(entity.getLatitude());
            double lng = Double.parseDouble(entity.getLongitude());
            LatLng position = new LatLng(lat, lng);
            polylineOptions.add(position);
            polyLineList.add(position);
            addMarkerToMap(position);
        }
        mMap.addPolyline(polylineOptions);

         animatorMarker = new AnimatorMarker(markers,mMap,polyLineList);
        animatorMarker.startAnimation(false);

    }
             private void addMarkerToMap(LatLng latLng) {
                 Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title("title").snippet("snippet").visible(false));
                 markers.add(marker);
             }

    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                     //   mapFragment.getMapAsync(this);

                    }

                } else {
                        finish();
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }



         }
